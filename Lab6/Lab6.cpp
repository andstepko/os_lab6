// Lab5.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <windows.h>
#include <stdio.h>
//#using <mscorlib.dll>

#include <iostream>
using namespace std;
#include <list>



struct AddressSize
{
	DWORD Address;
	DWORD Size;
};

void PrintAddressSize(AddressSize addressSize)
{
	_tprintf(_T("Address:   %x\n"), addressSize.Address);
	_tprintf(_T("Size:   %d\n"), addressSize.Size);
	_tprintf(_T("\n"));
}

void ShowSystemInfo()
{
	SYSTEM_INFO SystemInfo;
	MEMORYSTATUSEX MemoryStatus;

	GetSystemInfo (&SystemInfo);
	_tprintf(_T("������ ��������:   %d\n"), SystemInfo.dwPageSize);
	_tprintf(_T("����������� ����� ����������:   %d\n"),SystemInfo.lpMinimumApplicationAddress);
	_tprintf(_T("������������ ����� ����������:   %d\n"),SystemInfo.lpMaximumApplicationAddress);
	_tprintf(_T("�������������:   %d\n"), SystemInfo.dwAllocationGranularity);

	_tprintf(_T("\n"));
	_tprintf (_T("A������ ����������:   %d\n"),
		((DWORD)SystemInfo.lpMaximumApplicationAddress - (DWORD)SystemInfo.lpMinimumApplicationAddress));
	_tprintf (_T("������ � ������� ����������:   %d\n"),
		((DWORD)SystemInfo.lpMaximumApplicationAddress - (DWORD)SystemInfo.lpMinimumApplicationAddress)
			/ SystemInfo.dwAllocationGranularity);
	_tprintf(_T("\n"));

	MemoryStatus.dwLength = sizeof(MEMORYSTATUSEX);
	GlobalMemoryStatusEx (&MemoryStatus);
	_tprintf (_T("�������� ���������:   %I64d\n"), MemoryStatus.ullAvailPhys);
	_tprintf (_T("�������� ����������� �����:   %I64d\n"), MemoryStatus.ullAvailPageFile);
	_tprintf (_T("�������� ����������� ������:   %I64d\n"), MemoryStatus.ullAvailVirtual);
	_tprintf(_T("\n"));

	_tprintf (_T("����� ���������� ������:   %I64d\n"), MemoryStatus.ullTotalPhys);
	_tprintf (_T("����� ����������� ������:   %I64d\n"), MemoryStatus.ullTotalVirtual);
	_tprintf (_T("����� ����������� �����:   %I64d\n"), MemoryStatus.ullTotalPageFile);

	/*MEMORY_BASIC_INFORMATION Buffer;
	HANDLE hProcess = GetCurrentProcess();
	DWORD dwAddress = (DWORD)SystemInfo.lpMinimumApplicationAddress;
	while (dwAddress < (DWORD)SystemInfo.lpMaximumApplicationAddress)
	{
		DWORD dwSize = VirtualQueryEx(  hProcess, (const void *)dwAddress,
				&Buffer, sizeof (MEMORY_BASIC_INFORMATION));
	
		_tprintf (_T("BaseAddress = %#x\n"), dwAddress);
		_tprintf (_T("RegionSize = %u\n"), Buffer.RegionSize);
	
		switch(Buffer.State)
		{
			case MEM_COMMIT:_tprintf (_T("MEM_COMMIT\n_T(");break;
			case  MEM_FREE: _tprintf (_T("MEM_FREE\n_T(");break;
			case  MEM_RESERVE:_tprintf (_T("MEM_RESERVE\n_T(");break;
			default:_tprintf (_T("UNKNOWN\n_T(");
		}

		if (Buffer.State != MEM_FREE)
		{
			switch (Buffer.AllocationProtect)
			{
				case PAGE_READONLY: _tprintf (_T("PAGE_READONLY\n_T(");break;
				case PAGE_READWRITE: _tprintf (_T("PAGE_READWRITE\n_T(");
				break;
				case PAGE_EXECUTE: 	_tprintf (_T("PAGE_READWRITE\n_T(");break;
				case PAGE_EXECUTE_READ:
				_tprintf(_T("PAGE_EXECUTE_READ\n_T(");
				break;
				case PAGE_EXECUTE_READWRITE:
				_tprintf(_T("PAGE_EXECUTE_READWRITE\n_T(");
				break;	
				case PAGE_EXECUTE_WRITECOPY:
				_tprintf (_T("PAGE_EXECUTE_WRITECOPY\n_T(");
				break;
				case PAGE_NOACCESS: _tprintf (_T("PAGE_NOACCESS\n_T(");break;
				case PAGE_WRITECOPY: _tprintf (_T("PAGE_WRITECOPY\n_T(");
				break;
				default: _tprintf (_T("UNKNOWN\n_T(");
			}
		}
		_tprintf (_T("\n\n_T(");
		dwAddress+=Buffer.RegionSize;
	 }*/
}

list<AddressSize> BuildListOfBoxes()
{
	// Build list of free Memory blocks.
	list<AddressSize> result;
	SYSTEM_INFO SystemInfo;
	GetSystemInfo (&SystemInfo);

	MEMORY_BASIC_INFORMATION Buffer;
	HANDLE hProcess = GetCurrentProcess();
	DWORD dwAddress = (DWORD)SystemInfo.lpMinimumApplicationAddress;
	

	while (dwAddress < (DWORD)SystemInfo.lpMaximumApplicationAddress)
	{
		DWORD dwSize = VirtualQueryEx(  hProcess, (const void *)dwAddress,
				&Buffer, sizeof (MEMORY_BASIC_INFORMATION));
		AddressSize region;
		region.Address = dwAddress;
		region.Size = (DWORD)Buffer.RegionSize;

		if (Buffer.State == MEM_FREE)
		{
			result.push_back(region);
		}
		dwAddress += Buffer.RegionSize;
	}
	return result;
}

void ShowListOfFreeBlocks()
{
	list<AddressSize> freeBlocks;

	freeBlocks = BuildListOfBoxes();

	for(std::list<AddressSize>::iterator i = freeBlocks.begin(); i != freeBlocks.end(); i++)
	{
		AddressSize currRegion = *i;
		/*_tprintf(_T("Address:   %x\n"), currRegion.Address);
		_tprintf(_T("Size:   %d\n"), currRegion.Size);
		_tprintf(_T("\n"));*/
		PrintAddressSize(currRegion);
	}
}

AddressSize MinimalSufficient(DWORD sizeRequired)
{
	list<AddressSize> freeBlocks;
	DWORD minSize = MAXDWORD;
	DWORD address;
	AddressSize result;

	freeBlocks = BuildListOfBoxes();
	for(std::list<AddressSize>::iterator i = freeBlocks.begin(); i != freeBlocks.end(); i++)
	{
		AddressSize currRegion = *i;
		if((currRegion.Size < minSize) && (currRegion.Size >= sizeRequired))
		{
			minSize = currRegion.Size;
			address = currRegion.Address;
		}
	}
	result.Address = address;
	result.Size = minSize;

	return result;
}

void ShowMinmalSufficient(DWORD sizeRequired)
{
	AddressSize addressSize = MinimalSufficient(sizeRequired);
	PrintAddressSize(addressSize);
}

void AllocateMinSufficient(DWORD sizeRequired)
{
	AddressSize targetRegion = MinimalSufficient(sizeRequired);

	VirtualAlloc((LPVOID) targetRegion.Address, targetRegion.Size, MEM_COMMIT, PAGE_READONLY);
}

///3
int MyIndexOf(list<DWORD> inputList, DWORD inputValue)
{
	int count = -1;
	DWORD currValue;

	for(std::list<DWORD>::iterator i = inputList.begin(); i != inputList.end(); i++)
	{
		currValue = *i;

		if(currValue == inputValue)
		{
			return count;
		}
		count++;
	}
	if(count == inputList.size())
	{
		return -1;
	}
	return count;
}

void AddPageAlways(DWORD inputPageNumber, list<DWORD> inputMemory, DWORD inputMaxMemorySize)
{
	if(inputMemory.size() < inputMaxMemorySize)
	{
		// Write without removal.
	}
	else
	{
		// Remove first.
		inputMemory.pop_front();
	}
	inputMemory.push_back(inputPageNumber);
}

void WritePage(DWORD inputPageNumber, list<DWORD> inputMemory, DWORD inputMaxMemorySize)
{
	int index = MyIndexOf(inputMemory, inputPageNumber);
	
	if(index == -1)
	{
		// The page still doesn't exist.
	}
	else
	{
		// The page already exists.
		inputMemory.remove(inputPageNumber);
	}
	AddPageAlways(inputPageNumber, inputMemory, inputMaxMemorySize);
}

///4
DWORD GetIndexFromThreeBits(bool input[3])
{
	// Returns index, where to right.
	if(input[0])
	{
		// Write into RIGHT indexes, because last writing was into left indexes.
		if(input[2])
			return 3;
		else
			return 2;
	}
	else
	{
		// Write into LEFT indexes.
		if(input[1])
			return 1;
		else
			return 0;
	}
}

void SetThreeBits(DWORD inputIndex, bool* inputBits)
{
	int i;

	if(inputIndex > 1)
	{
		inputBits[0] = false;
		i = 2;
	}
	else
	{
		inputBits[0] = true;
		i = 1;
	}

	if(inputIndex % 2 > 0)
		inputBits[i] = false;
	else
		inputBits[i] = true;
}

DWORD LRUwriteAlways(DWORD* inputMemory, DWORD inputValue, bool* inputBits)
{
	DWORD index = GetIndexFromThreeBits(inputBits);

	inputMemory[index] = inputValue;
	SetThreeBits(index, inputBits);

	return index;
}

DWORD LRUwrite(DWORD* inputMemory, DWORD inputValue, bool* inputBits)
{
	// Writes page if it isn't sitll in memory and returrns the index of memory the page is currently settled.
	for(int i = 0; i < 4; i++)
	{
		if(inputMemory[i] == inputValue)
		{
			return i;
		}
	}
	return LRUwriteAlways(inputMemory, inputValue, inputBits);
}


int _tmain(int argc, _TCHAR* argv[])
{
	setlocale(LC_ALL, "Russian");

	ShowSystemInfo();
	//ShowListOfFreeBlocks();
	//ShowMinmalSufficient(20000);

	//AllocateMinSufficient(220000);




	/*std::list<DWORD> memory;

	WritePage(1, memory, 65000);
	WritePage(2, memory, 65000);
	WritePage(3, memory, 65000);
	WritePage(4, memory, 65000);
	WritePage(1, memory, 65000);
	WritePage(2, memory, 65000);
	WritePage(5, memory, 65000);
	WritePage(1, memory, 65000);
	WritePage(2, memory, 65000);
	WritePage(3, memory, 65000);
	WritePage(4, memory, 65000);
	WritePage(5, memory, 65000);*/






	/*bool bits[3];
	DWORD memory[4];

	LRUwrite(memory, 1, bits);
	LRUwrite(memory, 2, bits);
	LRUwrite(memory, 3, bits);
	LRUwrite(memory, 4, bits);
	LRUwrite(memory, 1, bits);
	LRUwrite(memory, 2, bits);
	LRUwrite(memory, 5, bits);
	LRUwrite(memory, 1, bits);
	LRUwrite(memory, 2, bits);
	LRUwrite(memory, 3, bits);
	LRUwrite(memory, 4, bits);
	LRUwrite(memory, 5, bits);*/

	getchar();

	return 0;
}

