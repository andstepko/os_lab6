========================================================================
    CONSOLE APPLICATION : Lab6 Project Overview
========================================================================

AppWizard has created this Lab6 application for you.

This file contains a summary of what you will find in each of the files that
make up your Lab6 application.


Lab6.vcxproj
    This is the main project file for VC++ projects generated using an Application Wizard.
    It contains information about the version of Visual C++ that generated the file, and
    information about the platforms, configurations, and project features selected with the
    Application Wizard.

Lab6.vcxproj.filters
    This is the filters file for VC++ projects generated using an Application Wizard. 
    It contains information about the association between the files in your project 
    and the filters. This association is used in the IDE to show grouping of files with
    similar extensions under a specific node (for e.g. _T(".cpp_T(" files are associated with the
    _T("Source Files_T(" filter).

Lab6.cpp
    This is the main application source file.

/////////////////////////////////////////////////////////////////////////////
Other standard files:

StdAfx.h, StdAfx.cpp
    These files are used to build a precompiled header (PCH) file
    named Lab6.pch and a precompiled types file named StdAfx.obj.

/////////////////////////////////////////////////////////////////////////////
Other notes:

AppWizard uses _T("TODO:_T(" comments to indicate parts of the source code you
should add to or customize.

/////////////////////////////////////////////////////////////////////////////
